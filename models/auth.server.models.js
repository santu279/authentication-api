var mongoose = require('mongoose')
var crypto = require('crypto')

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    index: {
      unique: true,
      sparse: true // For this to work on a previously indexed field, the index must be dropped & the application restarted.
    },
    lowercase: true,
    trim: true,
    default: '',
    required: 'please provide the valid email'
  },
  salt: {
    type: String
  },
  name: {
    type: String,
    unique: 'Name already exists',
    required: 'Please fill in a name',
    lowercase: true,
    trim: true
  },
  password: {
    type: String,
    default: ''
  },
  created: {
    type: Date,
    default: Date.now
  }
})
/**
 * Hook a pre save to hash the password
 */

userSchema.pre('validate', function (next) {
  if (this.password && this.isModified('password')) {
    this.salt = crypto.randomBytes(16).toString('base64')
    this.password = this.hashPassword(this.password)
  }
  next()
})

/**
 * Create instance method for hashing a password
 */
userSchema.methods.hashPassword = function (password) {
  if (this.salt && password) {
    return crypto
      .pbkdf2Sync(password, new Buffer(this.salt, 'base64'), 10000, 64, 'SHA1')
      .toString('base64')
  } else {
    return password
  }
}

/**
 * Create instance method for authenticating user
 */
userSchema.methods.authenticate = function (password) {
  return this.password === this.hashPassword(password)
}

module.exports = mongoose.model('userSchema', userSchema)

var express = require('express')
var validation = require('../middlewares')
var router = express.Router()
var user = require('../controller/index')

router.post('/create', validation.validateCreate, user.createUser)
router.post('/login', validation.validateLogin, user.signin)
router.post('/reset', validation.reset, user.reset)

module.exports = router

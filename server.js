var express = require('express')
var app = express()
var routes = require('./routes')
var bodyParser = require('body-parser')
var cors = require('cors')
var mongoose = require('./config/mongoose')
var dbo = mongoose()

// port on which server is up
const port = 3000

app.use(
  cors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204
  })
)

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/', routes)

app.listen(port, () => {
  console.log('server up')
})

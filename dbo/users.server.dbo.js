const Users = require('../models/auth.server.models')
const crypto = require('crypto')
exports.findUserByEmail = function (email, callback) {
  Users.findOne(
    {
      email: email
    },
    function (err, result) {
      if (result) {
        if (err) {
          callback('error')
        } else {
          callback(null, result)
        }
      } else {
        callback('user not found')
      }
    }
  )
}

exports.validatePassword = function (userData, password, callback) {
  if (userData.salt === null && userData.password === null) {
  } else {
    var password = crypto
      .pbkdf2Sync(
        password,
        new Buffer(userData.salt, 'base64'),
        10000,
        64,
        'SHA1'
      )
      .toString('base64')
    if (password === userData.password) {
      var data = {}
      data._id = userData._id
      data.username = userData.username
      data.role = userData.role
      data.email = userData.email
      data.firstName = userData.firstName
      data.lastName = userData.lastName
      data.activeStatus = userData.activeStatus
      data.isAllowed = userData.isAllowed
      callback(null, data)
    } else {
      callback('Password does not match')
    }
  }
}

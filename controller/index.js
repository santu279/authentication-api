// import { getToken } from '../helpers/jwt'
var jwt = require('../helpers/jwt')
var User = require('../models/auth.server.models')
var userDbo = require('../dbo/users.server.dbo')
var crypto = require('crypto')

const async = require('async')

var jwt = require('../helpers/jwt')

exports.createUser = function (req, res) {
  var data = req.body
  var user = new User(data)
  user.save(function (err, result) {
    if (err) {
      res.status(422).send({
        message: ' error while creating the user' + err,
        success: false
      })
    } else {
      res.status(201).send({
        message: 'user created successfully',
        success: true
      })
    }
  })
}

exports.signin = function (req, res) {
  const email = req.body.email
  const password = req.body.password
  async.waterfall(
    [
      function (next) {
        userDbo.findUserByEmail(email, next)
      },
      function (data, next) {
        userDbo.validatePassword(data, password, next)
      },
      function (data, callback) {
        callback(null, data)
      }
    ],
    function (err, result) {
      if (err) {
        res.status(400).send({
          success: false,
          message: 'Error while login' + err
        })
      } else {
        var access_token = jwt.getToken(result, 'access_token')
        var refresh_token = jwt.getToken(result, 'refresh_token')
        console.log('data inside the login', access_token, refresh_token)
        delete result.password
        delete result.salt
        var body = {
          access_token: access_token,
          refresh_token: refresh_token,
          userInfromation: result
        }
        res.status(200).send({
          response: body,
          message: 'Succesfully logged In'
        })
      }
    }
  )
}

exports.reset = function (req, res) {
  const passwordDetails = req.body.newPassword
  const currentPassword = req.body.currentPassword
  const email = req.body.email
  console.log('data inside the reset', email)
  userDbo.findUserByEmail(email, function (err, user) {
    if (err) {
      res.status(404).send({
        message: 'error finding the user',
        success: false
      })
    } else {
      async.waterfall(
        [
          function (next) {
            userDbo.validatePassword(user, currentPassword, next)
          },
          function (userData, callback) {
            callback(null, userData)
          }
        ],
        function (err, result) {
          if (err) {
            res.status(400).send({
              success: false,
              message:
                'validation error,  please provide the correct current password '
            })
          } else {
            var newPassword = crypto
              .pbkdf2Sync(
                passwordDetails,
                new Buffer(user.salt, 'base64'),
                10000,
                64,
                'SHA1'
              )
              .toString('base64')
            console.log('data inside the change password', newPassword)
            User.findOneAndUpdate(
              { email: email },
              { $set: { password: newPassword } },
              { new: true },
              function (err, place) {
                if (err) {
                  res.status(404).send({
                    message: 'failed to update the password',
                    success: false
                  })
                } else {
                  delete place.salt
                  delete place.password
                  res.status(200).send({
                    message: 'successfully updated the password',
                    success: true,
                    result: place
                  })
                }
              }
            )
          }
        }
      )
    }
  })
}

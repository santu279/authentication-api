const Joi = require('joi')
var validator = require('input-validator')

module.exports.validateCreate = function (req, res, next) {
  var valid = {}
  var createSchema = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 }),
    name: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
      .required(),
    password: Joi.string().required()
  })
  try {
    validator(req.body, createSchema)
    valid.isvalid = true
  } catch (e) {
    valid.isvalid = false
    valid.errors = e.message
  }
  if (valid.isvalid) next()
  else res.status(422).send(valid)
}

module.exports.validateLogin = function (req, res, next) {
  var valid = {}
  var signInSchema = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 }),
    password: Joi.string().required()
  })
  try {
    validator(req.body, signInSchema)
    valid.isvalid = true
  } catch (e) {
    valid.isvalid = false
    valid.errors = e.message
  }
  if (valid.isvalid) next()
  else res.status(422).send(valid)
}

module.exports.reset = function (req, res, next) {
  var valid = {}
  var resetSchema = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 })
  })
  try {
    validator(req.body, resetSchema)
    valid.isvalid = true
  } catch (e) {
    valid.isvalid = false
    valid.errors = e.message
  }
  if (valid.isvalid) next()
  else res.status(422).send(valid)
}

module.exports.me = function (req, res, next) {
  var valid = {}
  var meSchema = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 })
  })
  try {
    validator(req.body, meSchema)
    valid.isvalid = true
  } catch (e) {
    valid.isvalid = false
    valid.errors = e.message
  }
  if (valid.isvalid) next()
  else res.status(422).send(valid)
}

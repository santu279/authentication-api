var mongoose = require('mongoose')
var url = 'mongodb://localhost/authenticaton'

module.exports = function () {
  mongoose.Promise = global.Promise
  db = mongoose.connect(
    url,
    { useCreateIndex: true, useNewUrlParser: true }
  )
  return db
}
